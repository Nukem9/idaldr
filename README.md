# **Moved!** [New repository link](https://github.com/Nukem9/SwissArmyKnife) #

# Usage #

Plugin to load IDA's signature, diff, and linker map files in x64_dbg.

# Download #
https://bitbucket.org/Nukem9/idaldr/downloads