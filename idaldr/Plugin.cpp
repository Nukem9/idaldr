#include "stdafx.h"

int pluginHandle;
HWND hwndDlg;
int hMenu;

duint DbgGetCurrentModule()
{
	if (!DbgIsDebugging())
	{
		_plugin_printf("The debugger is not running!\n");
		return 0;
	}

	// First get the current code location
	SELECTIONDATA selection;

	if (!GuiSelectionGet(GUI_DISASSEMBLY, &selection))
	{
		_plugin_printf("GuiSelectionGet(GUI_DISASSEMBLY) failed\n");
		return 0;
	}

	// Convert the selected address to a module base
	duint moduleBase = DbgFunctions()->ModBaseFromAddr(selection.start);

	if (moduleBase <= 0)
	{
		_plugin_printf("Failed to resolve module base at address '0x%llX'\n", (ULONGLONG)selection.start);
		return 0;
	}

	return moduleBase;
}

bool OpenSelectionDialog(const char *Title, const char *Filter, bool Save, bool (*Callback)(char *, duint))
{
	duint moduleBase = DbgGetCurrentModule();

	if (moduleBase <= 0)
		return false;

	// Open a file dialog to select the map or sig
	char buffer[MAX_PATH];
	memset(buffer, 0, sizeof(buffer));

	OPENFILENAMEA ofn;
	memset(&ofn, 0, sizeof(OPENFILENAMEA));

	ofn.lStructSize = sizeof(OPENFILENAMEA);
	ofn.hwndOwner	= GuiGetWindowHandle();
	ofn.lpstrFilter = Filter;
	ofn.lpstrFile	= buffer;
	ofn.nMaxFile	= ARRAYSIZE(buffer);
	ofn.lpstrTitle	= Title;
	ofn.Flags		= OFN_FILEMUSTEXIST;

	if (Save)
	{
		ofn.lpstrDefExt = strchr(Filter, '\0') + 3;
		ofn.Flags		= OFN_OVERWRITEPROMPT;

		if (!GetSaveFileNameA(&ofn))
			return false;
	}
	else
	{
		if (!GetOpenFileNameA(&ofn))
			return false;
	}

	if (!Callback(buffer, moduleBase))
	{
		_plugin_printf("An error occurred while applying the file\n");
		return false;
	}

	return true;
}

bool ApplySignatureSymbols(char *Path, duint ModuleBase);
bool ApplyDiffSymbols(char *Path, duint ModuleBase);
bool ApplyMapSymbols(char *Path, duint ModuleBase);

bool ExportDiffSymbols(char *Path, duint ModuleBase);
bool ExportMapSymbols(char *Path, duint ModuleBase);

void MenuEntryCallback(CBTYPE Type, PLUG_CB_MENUENTRY *Info)
{
	switch (Info->hEntry)
	{
	case PLUGIN_MENU_LOADSIG:
		OpenSelectionDialog("Open a signature file", "Signatures (*.sig)\0*.sig\0\0", false, ApplySignatureSymbols);
		break;

	case PLUGIN_MENU_LOADDIF:
		OpenSelectionDialog("Open a DIF file", "Diff files (*.dif)\0*.dif\0\0", false, ApplyDiffSymbols);
		break;

	case PLUGIN_MENU_LOADMAP:
		OpenSelectionDialog("Open a MAP file", "Map files (*.map)\0*.map\0\0", false, ApplyMapSymbols);
		break;

	case PLUGIN_MENU_EXPORTDIF:
		OpenSelectionDialog("Save a DIF file", "Diff files (*.dif)\0*.dif\0\0", true, ExportDiffSymbols);
		break;

	case PLUGIN_MENU_EXPORTMAP:
		OpenSelectionDialog("Save a MAP file", "Map files (*.map)\0*.map\0\0", true, ExportMapSymbols);
		break;

	case PLUGIN_MENU_ABOUT:
		MessageBoxA(GuiGetWindowHandle(), "Plugin created by Nukem.\n\nSource code at:\nhttps://bitbucket.org/Nukem9/idaldr/", "About", 0);
		break;
	}

	//
	// Update GUI
	//
	GuiUpdateAllViews();
}

DLL_EXPORT bool pluginit(PLUG_INITSTRUCT *InitStruct)
{
	InitStruct->pluginVersion	= PLUGIN_VERSION;
	InitStruct->sdkVersion		= PLUG_SDKVERSION;
	pluginHandle				= InitStruct->pluginHandle;
	strcpy_s(InitStruct->pluginName, PLUGIN_NAME);

	// Add any of the callbacks
	_plugin_registercallback(pluginHandle, CB_MENUENTRY, (CBPLUGIN)MenuEntryCallback);
	return true;
}

DLL_EXPORT bool plugstop()
{
	_plugin_menuclear(hMenu);
	return true;
}

DLL_EXPORT void plugsetup(PLUG_SETUPSTRUCT *SetupStruct)
{
	hwndDlg = SetupStruct->hwndDlg;
	hMenu	= SetupStruct->hMenu;

	// Initialize the menu
	_plugin_menuaddentry(hMenu, PLUGIN_MENU_LOADSIG, "&Load SIG file");
	_plugin_menuaddentry(hMenu, PLUGIN_MENU_LOADDIF, "&Load DIF file");
	_plugin_menuaddentry(hMenu, PLUGIN_MENU_LOADMAP, "&Load MAP file");
	_plugin_menuaddseparator(hMenu);
	_plugin_menuaddentry(hMenu, PLUGIN_MENU_EXPORTDIF, "&Export DIF file");
	_plugin_menuaddentry(hMenu, PLUGIN_MENU_EXPORTMAP, "&Export MAP file");
	_plugin_menuaddseparator(hMenu);
	_plugin_menuaddentry(hMenu, PLUGIN_MENU_ABOUT, "&About");
}

BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	return TRUE;
}