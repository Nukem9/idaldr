#pragma once

#include <windows.h>
#include <stdio.h>
#include <vector>
#include <map>
#include <stdint.h>

#define PLUGIN_NAME		"IDALdr"
#define PLUGIN_VERSION	001

#ifdef _WIN64
#pragma comment(lib, "pluginsdk/x64_dbg.lib")
#pragma comment(lib, "pluginsdk/x64_bridge.lib")
#pragma comment(lib, "pluginsdk/TitanEngine/TitanEngine_x64.lib")
#pragma comment(lib, "pluginsdk/dbghelp/dbghelp_x64.lib")
#pragma comment(lib, "zlib/zlib_x64.lib")
#else
#pragma comment(lib, "pluginsdk/x32_dbg.lib")
#pragma comment(lib, "pluginsdk/x32_bridge.lib")
#pragma comment(lib, "pluginsdk/TitanEngine/TitanEngine_x86.lib")
#pragma comment(lib, "pluginsdk/dbghelp/dbghelp_x86.lib")
#pragma comment(lib, "zlib/zlib_x86.lib")
#endif

#define _plugin_printf(Format, ...) _plugin_logprintf("[" PLUGIN_NAME "] " Format, __VA_ARGS__)

#include "pluginsdk/_plugins.h"
#include "pluginsdk/bridgemain.h"
#include "pluginsdk/_dbgfunctions.h"
#include "pluginsdk/TitanEngine/TitanEngine.h"
#include "Plugin.h"

#include "IDA/Crc16.h"
#include "IDA/Sig.h"
#include "IDA/Diff.h"
#include "Map/Map.h"